# README #

### Info ###

	Taekwondo Platform.
	tkd_v1.0.

### Istruzioni ###

	Eseguire application/Launcher.class o il relativo eseguibile .jar per aprire l' applicazione e scegliere una delle tre attività:
	
	-Combattimento: inserire i nominativi dei due sfidanti e premere OK per iniziare una sessione di combattimento;
	-Forma: inserire i dati relativi allo studente e premere OK per iniziare una sessione di forma;
	-Esame: inserire almeno un alievo e un maestro e premere INIZIA per cominciare una sessione di esame.

### Team ###

	Riccardo Buzzolo, Dario Berettini, Luca Lamonaca, Mattia Ferrari.